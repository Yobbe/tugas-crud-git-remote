@extends('layout.master')
@section('judul')
Halaman Pendaftaran
@endsection

@section('isi')
    <form action="/kirim" method="post">
        @csrf
        <label>First Name</label> <br>
        <input type="text" name="nama"> <br> <br>
        <label>Last Name</label> <br> <br>
        <input type="text" name="nama"> <br><br>
        <label> Gander :</label> <br> <br>
        <input type="radio" name="M"> Male <br>
        <input type="radio" name="L"> Female <br>
        <input type="radio" name="O"> Other <br> <br>
        <label> Nationality :</label> <br> <br>
        <select name="NL">
            <option value="indonesia"> Indonesia</option>
            <option value="inggris"> Inggris</option>
        </select> <br> <br>
        <label> Language Spoken :</label> <br> <br>
        <input type="checkbox"> Bahasa Indonesia <br>
        <input type="checkbox"> English <br>
        <input type="checkbox"> Other <br><br>
        <label>Alamat</label> <br>
        <textarea name="address" id="" cols="30" rows="10"></textarea> <br> <br>
        <input type="submit" value="Sign Up">
@endsection