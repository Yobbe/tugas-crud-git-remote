<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CastController extends Controller
{
    public function create()
    {
        return view('kategori.create');
    }

    public function store(Request $request)
    {
        $request->validate(
            [
                'nama' => 'required',
                'umur' => 'required',
                'bio' => 'required',
            ],
            [
                'nama.required' => 'Nama Harus Diisi!',
                'umur.required' => 'Umur Harus Diisi!',
                'bio.required' => 'Bio Harus Diisi!',
            ]
        );

        DB::table('cast')->insert(
        [
            'nama' => $request['nama'],
            'umur' => $request['umur'],
            'bio' => $request['bio'],
        ]);

        return redirect('/cast');
    }

    public function index()
    {
        $cast = DB::table('cast')->get();
 
        return view('kategori.index', compact('cast'));
    }

    public function show($id)
    {
        $cast = DB::table('cast')->where('id', $id)->first();

        return view('kategori.show', compact('cast'));
    }

    public function edit($id)
    {
        $cast = DB::table('cast')->where('id', $id)->first();

        return view('kategori.edit', compact('cast'));
    }

    public function update(Request $request, $id)
    {
        $request->validate(
            [
                'nama' => 'required',
                'umur' => 'required',
                'bio' => 'required',
            ],
            [
                'nama.required' => 'Nama Harus Diisi!',
                'umur.required' => 'Umur Harus Diisi!',
                'bio.required' => 'Bio Harus Diisi!',
            ]
        );

        DB::table('cast')
              ->where('id', $id)
              ->update(
                [
                    'nama' => $request['nama'],
                    'umur' => $request['umur'],
                    'bio' => $request['bio'],
                ]
            );

            return redirect('/cast');
    }

    public function destroy($id)
    {
        DB::table('cast')->where('id', '=', $id)->delete();

        return redirect('/cast');
    }
}
